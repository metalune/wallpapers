## Sources:
### void
- void-simple.png https://github.com/KoalaV2/rice/blob/main/void_logo.png
- void-90s.png dunno
- void-material.png https://www.reddit.com/r/wallpapers/comments/98d8kc/a_void_linux_wallpaper/

### artix
- Artix_eko.jpg https://gitea.artixlinux.org/artix/artwork/src/branch/master/backgrounds/Artix_eko.jpg
- Artix_dna_spiral_dark.jpg https://gitea.artixlinux.org/artix/artwork/src/branch/master/backgrounds/Artix_dna_spiral_dark.jpg
- website/background-corona.png https://www.robalni.org/redeclipse/corona-background.png

### Notice
If any of those links are wrong or don't work anymore, please write an issue and correct it :)
If any links are not provided it is not because I don't wanna give the creators credit but simply
because I can't remember where I found the wallpaper.

